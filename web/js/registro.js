
$().ready(function(){
	validar();
	submitRegister();
});


function validar(){
	$('button').on('click', function(e){
		var input = {};
		$("input").each(function() {
			if($(this).val() == ""){
				$(this).addClass("error");
			}
			else{
				$(this).removeClass("error");
			}
		    input[$(this).attr("name")] = $(this).val();
		});
		validarPass(e, input);
		validarRFC(e, input);
		$('.error')[0].focus();
	});      
}

function validarPass(e, input){
	if(input.password !== input["pass-confirm"] || input.password == "" || input["pass-confirm"] == ""){
		console.log("no son iguales");
		$("input[name=password]").addClass("error");
		$("input[name=pass-confirm]").addClass("error");
		e.preventDefault();
	}
	else{
		$("input[name=password]").removeClass("error");
		$("input[name=pass-confirm]").removeClass("error");
	}
}

function validarRFC(e, input){
	if(!(input.rfc.length === 12 && isNaN(input.rfc[0]) && isNaN(input.rfc[1])  && isNaN(input.rfc[2]) && !isNaN(input.rfc.substr(3,6)))){//Estructura de RFC invalido
		console.log("error");
		$("input[name=rfc]").addClass("error");
		e.preventDefault();
	}
}

function submitRegister(){
	$('#register-form').on('submit', function(e){
		e.preventDefault();
		var elemento = $(this);
		$.post('php/registro.php', $(this).serialize(), function(data){
			console.log(data);
			if(data == "1"){
				window.location.href = "index.html";
			}
			else if(data.includes("Duplicate")){
				console.log("RFC duplicado!");
				e.preventDefault;
			}
			else{
				elemento.find("input[type=text], input[type=password], input[type=email]").val("");
			}
		});
		
	});
}