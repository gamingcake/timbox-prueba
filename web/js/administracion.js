$().ready(function(){
	navhover();
	$('.contenido').load("../html/home.html" , function(){
		loadHome();
	});	
	//cargar contenido
	$('li').click(function(){
		console.log($(this).attr("reg"));
		if($(this).attr("reg") === "empleado"){
			$('.contenido').load("../html/empleado.html" , function(){
				validar();
				loadSucursales($('#suc-sel'));
				submitEmpleado();
			});	
		}
		else if($(this).attr("reg") === "sucursal"){
			$('.contenido').load("../html/sucursal.html" , function(){
				validar();
				submitSucursal();
			});	
		}
		else if($(this).attr("reg") === "home"){
			$('.contenido').load("../html/home.html" , function(){
				loadHome();
			});	
		}
	});
});

function navhover(){
	$('li').hover(
		function(){
		$(this).addClass("active");
	}, function(){
		$(this).removeClass("active");
	});
}

function validar(){
	$('.btn-login').on('click', function(e){
		var input = {};
		$("input").each(function() {
			if($(this).val() == ""){
				$(this).addClass("error");
			}
			else{
				$(this).removeClass("error");
			}
		    input[$(this).attr("name")] = $(this).val();
		});
		if(input.rfc !== undefined){
		 	validarRFC(e, input);
		 }
		$('.error')[0].focus();
	});      
}

function validarRFC(e, input){
	if(!(input.rfc.length === 13 && isNaN(input.rfc[0]) && isNaN(input.rfc[1])  && isNaN(input.rfc[2]) && isNaN(input.rfc[3]) && !isNaN(input.rfc.substr(4,6)))){
	//Estructura de RFC invalido
		$("input[name=rfc]").addClass("error");
		e.preventDefault();
	}
}

function submitSucursal(){
	$('#sucursal-form').on('submit', function(e){
		e.preventDefault();
		$.post('../php/sucursal.php', $(this).serialize(), function(data){
			console.log(data);			
		});
		$(this).find("input[type=text], input[type=password], input[type=email], input[type=number]").val("");
	});
}

function submitEmpleado(){
	$('#empleado-form').on('submit', function(e){
		var elemento = $(this);
		e.preventDefault();
		$.post('../php/empleado.php', $(this).serialize(), function(data){
			if(data.includes("Duplicate")){
				console.log("RFC duplicado!");
				e.preventDefault;
			}
			else{
				elemento.find("input[type=text], input[type=password], input[type=email]").val("");
			}
		});
		
		
	});
}

function loadSucursales(select){
	$.get('../php/getSucursales.php', function(data){
		data = JSON.parse(data);
		var selecthtml = "";
		for(var i = 0; i < data.length; i++){
			selecthtml += '<option value="' + data[i].idsucursal + '">' + data[i].nombre + '</option>'
		}
		select.html(selecthtml);
	});
}

function loadHome(){
	$.get('../php/sucursalesCount.php', function(data){
		console.log(data);
		data = JSON.parse(data);
		if(data.length !== 0){
			var tablahtml = '<table class="table table-bordered table-hover table-home"><thead><tr><th>Sucursal</th><th>Empleados</th><th>Editar</th></tr></thead><tbody>';
			for(var i = 0; i < data.length; i++){
				tablahtml += '<tr class="datos"><td>' + data[i].sucursal + '</td><td>' + data[i].cantidad + '</td><td><button type="button" class="btn btn-md btn-primary btn-login btn-edit" value="' + data[i].idsucursal + '">Editar</button></td></tr>' ;
			}
			tablahtml += "</tbody></table>";
			$('#home-div').html(tablahtml);
			$('.btn-edit').click(function(){
				var valorBoton = $(this).val();
				$('.edit-suc-div').load("../html/EditarSucursal.html" , function(){
					validar();
					updateSucursal();
				});
				infoSucursal(valorBoton);
				

			});
		}
		else{
			$('#home-div').html("<h3 class='text-center'>No hay sucursales registradas.</h3>");
		}
	});	
}

function infoSucursal(sucId){
	var send ={"sucursal": sucId};
	$.post('../php/infoSucursal.php',send, function(data){
		data = JSON.parse(data);
		var sucInfo = data.sucursalInfo[0];
		$("#editar-sucursal-form input[name=nombre]").val(sucInfo.nombre);
		$("#editar-sucursal-form input[name=direccion]").val(sucInfo.direccion);
		$("#editar-sucursal-form input[name=colonia]").val(sucInfo.colonia);
		$("#editar-sucursal-form input[name=cp]").val(sucInfo.codigoPostal);
		$("#editar-sucursal-form input[name=ciudad]").val(sucInfo.ciudad);
		$("#editar-sucursal-form input[name=pais]").val(sucInfo.pais);

		var empleados = data.empleadosSuc;

		agregarTablaEmpleados(empleados);
		
	});
}

function agregarTablaEmpleados(empleados){
	var empleadosHtml = '<table class="table table-bordered table-hover table-home"><thead><tr><th>Empleado</th><th>RFC</th><th>Puesto</th><th>Editar</th></tr></thead><tbody>';
	for(var i = 0; i < empleados.length; i++){
		empleadosHtml += '<tr class="datos"><td>' + empleados[i].nombre + '</td><td>' + empleados[i].RFC + '</td><td>' + empleados[i].puesto + '</td><td><button type="button" class="btn btn-md btn-primary btn-login btn-empleado" value="' + i + '">Editar</button></td></tr>' ;
	}
	empleadosHtml += "</tbody></table>";
	$('#tabla-emp-div').html(empleadosHtml);
	edicionEmpleado(empleados);	
}

function edicionEmpleado(empleados){
	$('.btn-empleado').click(function(){
		var empInfo = empleados[$(this).val()];	
		$('.edit-emp-div').load("../html/editarEmpleado.html", function(){
			validar();
			
			$("#editar-empleado-form input[name=nombre]").val(empInfo.nombre);
			$("#editar-empleado-form input[name=rfc]").val(empInfo.RFC);
			$("#editar-empleado-form input[name=rfc]").prop('disabled', true);
			$("#editar-empleado-form input[name=puesto]").val(empInfo.puesto);
			loadSucursales($('#suc-sel'));
			updateEmpleado(empInfo);
		});
	});
}

function updateSucursal(){
	$('#editar-sucursal-form').on('submit',function(){
		$.post('../php/updateSucursal.php', $(this).serialize(), function(data){
			console.log(data);			
		});
	});
}

function updateEmpleado(infoEmpleado){

	$('#editar-empleado-form').on('submit',function(){
		var info = $(this).serialize() + "&id=" +  infoEmpleado.idempleado;
		$.post('../php/updateEmpleado.php', info, function(data){
			console.log(data);			
		});
	});
}