<?php 
	include 'connect.php';
	$con = conectarBD();
	session_start();
	$empresa = mysqli_real_escape_string($con, $_SESSION['idEmpresa']);
	

	$queryStr = "SELECT 
    sucursales.nombre as sucursal,
    COUNT(rfc) as cantidad,
    sucursales.idsucursal
FROM
    timbox.empleado
        LEFT JOIN
    sucursales ON empleado.idsucursal = sucursales.idsucursal
WHERE
    sucursales.idempresa = $empresa
GROUP BY sucursales.idsucursal;";

    $res = query($con, $queryStr);

    
    echo json_encode($res);
	cerrarCon($con);
 ?>