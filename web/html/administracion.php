<?php 
	session_start();
	if(!isset($_SESSION['nombre'])){
		header("Location: ../index.html");
	}	
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Administracion</title>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<!-- JS -->
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/administracion.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">Sistema empresas</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li reg="home"><a href="#">Home</a></li>
	      <li id="sucursal" reg="sucursal"><a href="#">Registrar Sucursal</a></li>
	      <li id="empleados" reg="empleado"><a href="#">Registrar Empleados</a></li>
	    </ul>
	  </div>
	</nav>
	<h1 class="text-center">Bienvenido <?php echo $_SESSION['empresa'] ?></h1>
	<h2 class="text-center"><?php echo $_SESSION['nombre'] ?></h2>

	<div class="container-fluid contenido">
		
	</div>
	

</body>
</html>